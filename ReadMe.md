--News Of The World--

By taking advantage of NLTK, RAKE, and TF-IDF, we are able to post articles and receive keywords paired with relevance scores.

News of the World receives a user-input keyword, and scans a large database of RSS feeds to see if any articles have a significant relationship to that article.
If it does, we mark the article, and drop a pin on a map where the headquarters of that news channel is located.

This allows a user to view different perspective on certain topics all around the world.

For example:

User searches for the word "Zika"...
-> Since Zika is more of a problem for the Western Hemisphere, a lot more pins would pop up for news stations on this side of the planet. The user
can then read articles from the perspective of news stations in South America countries, vs ones in the United States.

*Gain some perspective*


Setup dependencies (from fresh Ubunutu 16.04 install):

apt-get install:
    mongodb

pip3:
- newspaper3k (may require pip package setuptools, and apt-get libxml2-dev and libxslt1-dev)
- nltk
- numpy
- django==1.10
- mongoengine==0.9.0
- pymongo==2.8


from python shell:
import nltk
nltk.download()
d averaged_perceptron_tagger
d stopwords
d punkt

for mongo service:
    cp dist/mongod.conf /etc/
    cp dist/mongodb.service /etc/systemd/system/
