
var modal = document.getElementById('myModal');
var btn = document.getElementById("myBtn")

var ShowClickedArticle = function(data)
{
  renderArticle(data)
  // debugger
  // myHilitor.apply(data.keywords);
}

var HighlightKeywords = function(data)
{

  var words = ""
  for (var i = 0, len = data['keywords'].length; i < len; i++)
  {
    words += data['keywords'][i]
    words += " "
  }
  console.log("words: " + words)
  var hi = new Hilitor("myModal")  
  hi.apply(words)
}

$(document).ready(function() {
  $(document).on('click', '.carousel-inner', function(event)
    {
      // var title = $(this).attr("data-title")
      thi = $(this)
      query_index = thi.find(".active").index()
      var query = {'curr_index' : String(query_index)};
      var result = $.get("/perspective/search/",query,ShowClickedArticle).fail(function() {
                alert( "Error fetching!" );
            })
      {
      }

      
      // JSON.parse(result)
      // console.log(result['text'])
    });
});

var currData = null;

var init = {  "AK": {"count": 0},
              "AL": {"count": 0},
              "AR": {"count": 0},
              "AZ": {"count": 0},
              "CA": {"count": 0},
              "CO": {"count": 0},
              "CT": {"count": 0},
              "DE": {"count": 0},
              "FL": {"count": 0},
              "GA": {"count": 0},
              "HI": {"count": 0},
              "ID": {"count": 0},
              "IL": {"count": 0},
              "IN": {"count": 0},
              "IA": {"count": 0},
              "KS": {"count": 0},
              "KY": {"count": 0},
              "LA": {"count": 0},
              "MD": {"count": 0},
              "ME": {"count": 0},
              "MA": {"count": 0},
              "MN": {"count": 0},
              "MI": {"count": 0},
              "MS": {"count": 0},
              "MO": {"count": 0},
              "MT": {"count": 0},
              "NC": {"count": 0},
              "NE": {"count": 0},
              "NV": {"count": 0},
              "NH": {"count": 0},
              "NJ": {"count": 0},
              "NY": {"count": 0},
              "ND": {"count": 0},
              "NM": {"count": 0},
              "OH": {"count": 0},
              "OK": {"count": 0},
              "OR": {"count": 0},
              "PA": {"count": 0},
              "RI": {"count": 0},
              "SC": {"count": 0},
              "SD": {"count": 0},
              "TN": {"count": 0},
              "TX": {"count": 0},
              "UT": {"count": 0},
              "WI": {"count": 0},
              "VA": {"count": 0},
              "VT": {"count": 0},
              "WA": {"count": 0},
              "WV": {"count": 0},
              "WY": {"count": 0},
}


// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

btn.onclick = function() 
{
  modal.style.display = "block";
}

var popupInfo = function(geography, data) {
    return '<div class="hoverinfo">' + geography.properties.name +
           '<br>Number of Results: ' +  data.count + '</div>'
}

var renderArticle = function(data)
{
  $('#myModal').html(data);
    modal.style.display = "block";
    query_index = thi.find(".active").index()
    query = {'curr_keywords' : String(query_index)};
      var result = $.get("/perspective/search/",query,HighlightKeywords).fail(function() {
                alert( "Error fetching!" );
            })
}

var renderResults = function(data) {
    $('#results-container').html(data);
    $('html,body').animate({
        scrollTop: $('#results-container').offset().top-100
    },750);
    $("#results").carousel({
        autoPlay: 3000, //Set AutoPlay to 3 seconds
    })
}

var stateClicked = function(datamap) {
  var query = null
    datamap.svg.selectAll('.datamaps-subunit').on('click', function(geography) {
        if (!$('#search').val())
            alert("Enter a keyword!");
        else{
            var query = {'q': $('#search').val(), 's':geography.id};
            var results = $.get("/",query,renderResults).fail(function() {
                alert( "Error fetching!" );
            })
        }
    });
}


//basic map config with custom fills, mercator projection
var map = new Datamap({
            scope: 'usa',
            element: document.getElementById('map'),
            done: stateClicked,
            geographyConfig: {
                highlightBorderColor: '#bada55',
                highlightBorderWidth: 3,
                popupTemplate: popupInfo,
            },
            projection: 'mercator',
            fills: {
                defaultFill: '#D3D3D3',
                highlighted: '#f08080',
            },
            data: init,
            responsive: true,
        });
        window.addEventListener('resize',function(event){
            map.resize();});

var keywordSearchResult = function(data){
    var total = data['total'];
    if (total == null) total = '0';
    $('#number-results').html(total + " results found");
    console.log("total: " + total);
    delete data['total'];

    var noResults = total == 0;

    var counts = {};
    var maxCount = 0;
    var color;
    var stateCount;
    for (state in data){
        maxCount = Math.max(maxCount, data[state]);
    }
    for (state in init){
        if (!noResults && state in data){
            stateCount = data[state];
            color = getColor(stateCount/maxCount);
            counts[state] = { color, count : stateCount};
        }
        else {
            counts[state] = { count: 0, fillKey: 'defaultFill' };
        }
    }
    map.updateChoropleth(counts);
    if (!noResults){
        $('html,body').animate({
            scrollTop: $('#map').offset().top
        },750);
    }
}

var submit = function(){
    $('#results-container').html("");
    var query = {'q': $('#search').val()};
    var results = $.get("/",query,keywordSearchResult).fail(function() {
        alert( "Error fetching!" );});
};

$('#keybutton').click(submit);
$('#search').keypress(function(e){
    if (e.which == '13')
        submit();
    }
);

function getColor(percent) {
    var r = 255*percent
    var g = 255-r
    return 'rgb('+r+','+g+',0)'
  }

/*  js.usmap stuff - shelved for now
$(document).ready(function() {
    $('#map2').usmap({});
  });
$('#map2').usmap({
  // The click action
  click: function(event, data) {
    $('#clicked-state')
      .text('You clicked: '+data.name)
      .parent().effect('highlight', {color: '#C7F464'}, 2000);
  }
});
*/

// Original JavaScript code by Chirp Internet: www.chirp.com.au
// Please acknowledge use of this code by including this header.

function Hilitor(id, tag)
{

  var targetNode = document.getElementById(id) || document.body;
  var hiliteTag = tag || "EM";
  var skipTags = new RegExp("^(?:" + hiliteTag + "|SCRIPT|FORM|SPAN)$");
  var colors = ["#ff6"];
  var wordColor = [];
  var colorIdx = 0;
  var matchRegex = "";
  var openLeft = false;
  var openRight = false;

  this.setMatchType = function(type)
  {
    switch(type)
    {
      case "left":
        this.openLeft = false;
        this.openRight = true;
        break;
      case "right":
        this.openLeft = true;
        this.openRight = false;
        break;
      case "open":
        this.openLeft = this.openRight = true;
        break;
      default:
        this.openLeft = this.openRight = false;
    }
  };

  this.setRegex = function(input)
  {
    input = input.replace(/^[^\w]+|[^\w]+$/g, "").replace(/[^\w'-]+/g, "|");
    input = input.replace(/^\||\|$/g, "");
    if(input) {
      var re = "(" + input + ")";
      if(!this.openLeft) re = "\\b" + re;
      if(!this.openRight) re = re + "\\b";
      matchRegex = new RegExp(re, "i");
      return true;
    }
    return false;
  };

  this.getRegex = function()
  {
    var retval = matchRegex.toString();
    retval = retval.replace(/(^\/(\\b)?|\(|\)|(\\b)?\/i$)/g, "");
    retval = retval.replace(/\|/g, " ");
    return retval;
  };

  // recursively apply word highlighting
  this.hiliteWords = function(node)
  {
    if(node === undefined || !node) return;
    if(!matchRegex) return;
    if(skipTags.test(node.nodeName)) return;

    if(node.hasChildNodes()) {
      for(var i=0; i < node.childNodes.length; i++)
        this.hiliteWords(node.childNodes[i]);
    }
    if(node.nodeType == 3) { // NODE_TEXT
      if((nv = node.nodeValue) && (regs = matchRegex.exec(nv))) {
        if(!wordColor[regs[0].toLowerCase()]) {
          wordColor[regs[0].toLowerCase()] = colors[colorIdx++ % colors.length];
        }

        var match = document.createElement(hiliteTag);
        match.appendChild(document.createTextNode(regs[0]));
        match.style.backgroundColor = wordColor[regs[0].toLowerCase()];
        match.style.fontStyle = "inherit";
        match.style.color = "#000";

        var after = node.splitText(regs.index);
        after.nodeValue = after.nodeValue.substring(regs[0].length);
        node.parentNode.insertBefore(match, after);
      }
    };
  };

  // remove highlighting
  this.remove = function()
  {
    var arr = document.getElementsByTagName(hiliteTag);
    while(arr.length && (el = arr[0])) {
      var parent = el.parentNode;
      parent.replaceChild(el.firstChild, el);
      parent.normalize();
    }
  };

  // start highlighting at target node
  this.apply = function(input)
  {
    this.remove();
    if(input === undefined || !input) return;
    if(this.setRegex(input)) {
      this.hiliteWords(targetNode);
    }
  };

}
