var loginSuccess = function(data) {
    console.log(data);
    if ('Success!' == data){
        window.location.replace("../");
    }
    else{
        $('#notification').html(data);
    }
}
var login = function(){
    var query = { 'uname': $('#username').val(),
                  'pwd' : $('#password').val()
    };
    var results = $.get("/login",query,loginSuccess).fail(function() {
        alert( "Error fetching!" );
    });
}
$('#loginbutton').click(login);

var createSuccess = function(data) {
    console.log(data)
    $('#notification').html(data);
    if (data == 'Username Created!'){
        login();
    }
}
$('#createbutton').click(function(){
    var query = { 'uname': $('#username').val(),
                  'pwd' : $('#password').val(),
                  'first' : $('#first').val(),
                  'last' : $('#last').val(),
                  'email' : $('#email').val()
    };
    var results = $.get("../create",query,createSuccess).fail(function(data) {
        alert( "Error fetching!" );
    })
});

