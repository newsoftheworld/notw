from mongoengine import *

class Article(Document):
    date_added = DateTimeField()
    keywords = ListField(StringField(max_length=100))
    state_code = StringField(max_length=5)
    title = StringField(max_length=200)
    link = StringField(max_length=200)
    text = StringField(max_length=100000)
    image = StringField()
    video = BooleanField()

if __name__=="__main__":
    connect('notw')
    import pdb
    pdb.set_trace()
    pass
