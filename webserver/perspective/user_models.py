from mongoengine import *
from mongoengine.django.auth import User
class Auth:
    def __init__(self, idf):
        self.val = int(str(idf),16)
    def value_to_string(self,user):
        return str(hex(self.val))[2:]
    def __int__(self):
        return self.val

class Profile(User):
    #TODO - ask for location. We can track trends this way
    location = GeoPointField()
    number_logins = IntField()
    searches = ListField(StringField())
    def __init__(self,*args,**kwargs):
        super(Profile,self).__init__(*args,**kwargs)
        self._meta.pk = Auth(self.id) if self.id else None

def get_profile(request):
    key = "_auth_user_id"
    if key in request.session:
        profile = Profile.objects(id=request.session[key])
        if profile.count()==1:
            return profile[0]
    return None
