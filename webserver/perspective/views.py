import pdb
import mongoengine
from mongoengine.queryset import DoesNotExist

from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.contrib.auth import authenticate, login, logout
from perspective.models import *
from perspective.user_models import *
import operator
from datetime import datetime, timedelta
SESSION_KEY = "_auth_user_id"

# Main function for processing both the map coloring and the article results
def search(request):
    global currData
    q_keyword = request.GET.get('q')
    q_state = request.GET.get('s')
    q_index = request.GET.get('curr_index')
    q_keywords = request.GET.get('curr_keywords')
    context = {}
    inner_context = {}
    if q_keyword:
        q_keyword=q_keyword.lower()
    current_user = get_profile(request)
    keyword = request.GET.get('q')
    state = request.GET.get('s')
    days = request.GET.get('hist')
    history = datetime.now() - timedelta(days=int(days)) if days else datetime.now()-timedelta(days=1)
    if keyword:
        keyword=keyword.lower()
        # if there is both the keyword and state params we want
        # to return the list of articles for the requested state
        if state:
            art_list = Article.objects(keywords=keyword,state_code=state,date_added__gte=history).order_by('-date_added')
            currData = art_list
            print("currData set = " + str(currData))
            length = art_list.count()
            if length > 0:
                context['art_list'] = art_list#[start:end]
                context['additional'] = range(1,length)
            return render(request, 'perspective/results.html', context)
        # otherwise we want to return the data to populate the map
        else:
            if current_user:
                current_user.searches.append(keyword)
                current_user.save()
            result_set = Article.objects(keywords=keyword,date_added__gte=history)
            total = result_set.count()
            if(total>0):
                counts = {'total' : total}
                for state in result_set.distinct('state_code'):
                    counts[state] = result_set(state_code=state).count()
                return JsonResponse(counts)
            else:
                return HttpResponse("No Match!")
    if q_index:
        print ("q_index:" + q_index)
        print ("selected data:" + str(currData[int(q_index)].title))
        print ("keywords" + str(currData[int(q_index)].keywords))
        inner_context['text'] = currData[int(q_index)].text
        inner_context['link'] = currData[int(q_index)].link
        inner_context['title'] = currData[int(q_index)].title
        context['left_art'] = inner_context
        # pdb.set_trace()
        return render(request, 'perspective/comparison.html', context)
    if q_keywords:
        context["keywords"] = currData[int(q_keywords)].keywords
        return JsonResponse(context)
    # if no parameters were passed in then render the page
    else:
        context = {}
        if current_user: context['username'] = current_user.username
        return render(request, 'perspective/search.html', context)

# class Article(Document):
#     keywords = ListField(StringField(max_length=100))
#     state_code = StringField(max_length=5)
#     title = StringField(max_length=200)
#     link = StringField(max_length=200)
#     text = StringField(max_length=100000)
#     image = StringField()
#     video = BooleanField()
def fix_req_for_auth(request):
    if SESSION_KEY in request.session:
        request.session[SESSION_KEY] = int(Auth(request.session[SESSION_KEY]))

def create_username(request):
    try:
        name = request.GET.get('uname')
        pwd = request.GET.get('pwd')
        email = request.GET.get('email')
        first =  request.GET.get('first')
        last =  request.GET.get('last')
        profile = Profile.create_user(username=name,password=pwd,email=email)
        profile.first_name = first
        profile.last_name = last
        profile.number_logins = 0
        profile.save()
        return HttpResponse('Username Created!')
    except mongoengine.errors.NotUniqueError:
        return HttpResponse('Username in Use!')
    except mongoengine.errors.ValidationError as e:
        return HttpResponse(e.message)

def log_in(request):
    name = request.GET.get('uname')
    pwd = request.GET.get('pwd')
    if name:
        try:
            user = authenticate(username=name,password=pwd)
            if user is not None:
                assert isinstance(user, mongoengine.django.auth.User)
                request.session.set_expiry(60 * 60 * 1)
                fix_req_for_auth(request)
                login(request,user)
                user.number_logins += 1
                user.save()
                return HttpResponse('Success!')
            else: return HttpResponse('Failed to authenticate!')
        except DoesNotExist:
            return HttpResponse('User does not exist!')
    else:
        return render(request, 'perspective/login.html')

def about(request):
    current_user = get_profile(request)
    context = {}
    if current_user: context['username'] = current_user.username
    return render(request, 'perspective/about.html', context)
