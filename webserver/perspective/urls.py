from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'login', views.log_in, name='login'),
    url(r'create', views.create_username, name='create'),
    url(r'about', views.about, name='about'),
    url(r'',views.search,name='search')
]
