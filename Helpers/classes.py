import pdb
import queue
import os,sys
sys.path.append(os.getcwd()+'/AI/RAKE/')
sys.path.append(os.getcwd()+'/Resources/')
from RAKEAlgo import *
import terms
import nltk
import newspaper # for getting articles from websites
import feedparser # for rss reading
import importer
import logging
from logging.config import dictConfig
import logging.handlers
from stoppable_thread import StoppableThread
import threading
import time
import math
from webserver.perspective.models import *
from datetime import datetime

logging.config.fileConfig("Logging/logging_config.conf")

class DataElement:
    def __init__(self, state_code, article):
        # logging.info("new data element")
        image_link, is_video = self.try_parse_header_image(article)
        text=article.description
        self.article = Article(date_added=datetime.now(),state_code=state_code,title=article.title,link=article.link,text=text,image=image_link,video=is_video)
        self.title_analysis_thread = threading.Thread(name='Title_Analysis_Thread', target=self.tokenize_title)
        self.proper_nouns_analysis_thread = threading.Thread(name='Proper_Nouns_Analysis_Thread', target=self.check_for_proper_nouns, args=(text,))
        self.proper_nouns_analysis_thread.start()
        self.title_analysis_thread.start()
        # raked_words = rake.extract(text, incl_scores=True)[0:25]

        # sentences = [nltk.pos_tag(sent) for sent in sentences]
        # the above is currently ommited because it takes FUCKING FOREVER
        # put some algorithm here to see what we can make of the tokenized sentences with POS to add
        # weights and such to the keyword manually.
        # self.keywords.append(rake.extract(text, incl_scores=True)[0:15]) # Get keywords and take the top ones

    # Instead of calling the appending inline, this method checks for duplicates and other stuff.
    # It also allows it to be handled from outside the class.
    # def add_keyword(self, keyword, weight = 2.0):
        # keyword = keyword.lower()
        # if keyword in self.keywords.keys():
        #     self.keywords[keyword] = self.keywords[keyword] * 1.2 # Increase its weight if it exists
        # else:
        #     self.keywords[keyword] = weight

    def try_parse_header_image(self, article):
        links = article.links
        # assume the header image is the first image TODO find a better way?
        parsed=""
        is_video = False
        if 'media_content' in article:
            content = article.media_content[0]
            parsed = article.media_content[0]['url']
            if 'type' in content:
                media_type = article.media_content[0]['type']
                is_video = 'video' in media_type
            elif 'medium' in content:
                media_type = article.media_content[0]['medium']
                is_video = 'video' in media_type
            else:
                is_video = 'VID=' in parsed
        elif 'media_thumbnail' in article:
            parsed = article['media_thumbnail'][0]['url']
            is_video = False
        else:
            for link in links:
                try:
                    if 'video' in link['type']:
                        is_video = True
                        parsed = link['href']
                    elif 'image' in link['type']:
                        parsed = link['href']
                except:
                    continue
        return parsed, is_video

    def tokenize_title(self):
        # logging.info("In tokenize_title()")
        for token in nltk.word_tokenize(self.article.title): # Make the title a searchable list of phrases
            if token not in terms.punctuation() and token not in terms.stopWords(): # and make sure the title doesn't have crap
                # 2.0 is given for a title, words in the title are HUGELY important. Normal words get their tf-idf scores + repetition scores
                self.article.keywords.append(token.lower())
        # rake = RakeKeywordExtractor()

    def check_for_proper_nouns(self, text): # Should be operating on its own thread
        # logging.info("In check_for_proper_nouns()")
        sentences = nltk.sent_tokenize(text) # Get text
        sentences = [nltk.word_tokenize(sent) for sent in sentences] # Get sentences again? Not sure why this is necessary

        for sentence in sentences:
            pos_tag_sentence = nltk.pos_tag(sentence)
            for pos_tagged_word in pos_tag_sentence:
                if(pos_tagged_word[1] == "NNP" and pos_tagged_word[0] not in terms.punctuation() and pos_tagged_word[0] not in terms.stopWords()):
                    # logging.info("Found NNP " + pos_tagged_word[0])
                    self.article.keywords.append(pos_tagged_word[0].lower())

    def save(self):
        # make sure that the analysis threads are complete
        self.proper_nouns_analysis_thread.join()
        self.title_analysis_thread.join()
        self.article.save()

class RssScanner:
    def __init__(self):
        self.data_elements = []
        self.parse_thread = StoppableThread(target=self.parse_feeds,name='Article Parse Thread')
        self.keywordize_thread = StoppableThread(target=self.create_data_elements,name='Keywordize Thread')
        self.client = connect('notw')
        self.db = self.client.notw

    def import_feeds(self):
        # logging.debug("Going to import feeds()...")
        return importer.load_sites() # Reads the sites from the states list in SiteByState

    def scan_feeds_threaded(self):
        self.rss_feeds = queue.Queue()
        self.parse_thread.start()
        self.keywordize_thread.start()

    def parse_feeds(self):
        feeds = self.import_feeds()
        logging.info("started the scan of raw data from rss feeds")
        while(True):
            for location in feeds.keys():
                for rss_link in feeds[location]:
                    if self.parse_thread.stopped():
                        break
                    time.sleep(0.1)
                    parsed_feed = feedparser.parse(rss_link)
                    parsed_feed['location']=location
                    self.rss_feeds.put(parsed_feed)
                if self.parse_thread.stopped():
                    break
            if self.parse_thread.stopped():
                break
        logging.info("Exiting parse feeds thread...")

    def create_data_elements(self):
        article_analyzed = {}
        while(True):
            if self.keywordize_thread.stopped():
                break
            try:
                feed = self.rss_feeds.get(timeout = 5)
            except queue.Empty:
                logging.info('No feeds updated before timeout')
                continue

            for article in feed.entries:
                if self.keywordize_thread.stopped():
                    break
                if 'link' in article and article.link not in article_analyzed:
                    # avoid dupes!!
                    # for constant lookup by string - check that we didn't analyze in this run
                    article_analyzed[article.link] = 0
                    # check that we didn't analyze in previous runs in DB - one time linear :(
                    if Article.objects(link=article.link).count()>0:
                        article_analyzed[article.link] = 0
                    else:
                        try:
                            new_element = DataElement(feed['location'],article)
                            # blocks on join of analysis threads
                            new_element.save()
                        except Exception as e:
                            inf = sys.exc_info()
                            logging.error("Article creation error - {}: {}".format(inf[0],inf[1]))
        logging.info("Exiting create data elements thread...")

    def get_data_elements(self):
        return self.data_elements

    def kill(self):
        logging.info('Killing scanner...')
        # Keep in this order. getter should kill before setter
        self.keywordize_thread.stop()
        self.parse_thread.stop()
        self.parse_thread.join()
        self.keywordize_thread.join()
