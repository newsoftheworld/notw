import json
import logging
from logging.config import dictConfig
import logging.handlers

logging.config.fileConfig("Logging/logging_config.conf")
# import os

def load_sites():
	logging.debug("In load_sites() in  importer")
	f = open("Resources/SiteByState.json")
	json_dict = json.loads(f.read())
	# print(os.getcwd())
	return json_dict 
