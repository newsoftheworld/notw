import os,sys
sys.path.append(os,getcwd()+'/Resources/')
sys.path.append(os.getcwd()+'/Helpers/')
sys.path.append(os.getcwd()+'/APIs/Twitter/')
import operator
import nltk
import string
import pdb
import importer
from classes import *
from apscheduler.schedulers.blocking import BlockingScheduler
from TwitterTest import TwitterAccessor


# import sched, time
# s = sched.scheduler(time.time, time.sleep)
# def do_something(sc):
#     print "Doing stuff..."
#     # do your stuff
#     sc.enter(60, 1, do_something, (sc,))

# s.enter(60, 1, do_something, (s,))
# s.run()


class Brain:
	def __init__(self):
		self.twitter_acc = TwitterAccessor()
		self.scheduler = BlockingScheduler()
		self.scheduler.add_job(self.keep_social_media_terms_updated, 'interval', hours=1)
		self.scheduler.start()

	def get_current_location(): # returns WOEID
		return 23424977 # Currently only care about the united states

	def keep_social_media_terms_updated():
		print("Social Media!")

	def return_best_answers_by_state_code(self, keyphrase):
		# for each tokenized word in keyphrase, search and find the best articles and their state code
