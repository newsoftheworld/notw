# Adapted from: github.com/aneesha/RAKE/rake.py
from __future__ import division
import pdb
import os, sys
sys.path.append(os.getcwd()+'/Resources/')
sys.path.append(os.getcwd()+'/Helpers/')
sys.path.append(os.getcwd()+'/Logging')
import operator
import nltk
import string
from newspaper import Article # for getting articles from websites
import feedparser # for rss reading
import importer
from classes import *
import logging
import logging.handlers
from logging.config import dictConfig
import time

def start_query_service():
    logging.config.fileConfig("Logging/logging_config.conf")
    logging.info("Started new process...")
    scanner = RssScanner()
    scanner.scan_feeds_threaded()
    data = scanner.get_data_elements()
    try:
        while(True):
            time.sleep(1)
    except(KeyboardInterrupt):
        logging.info('Process aborted. Later!')
    finally:
        scanner.kill()

if __name__ == "__main__":
    start_query_service()
    logging.info('Exiting program...')
    sys.exit(0)
